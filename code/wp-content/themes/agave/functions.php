<?php

require "includes/login/class-login_filters.php";

add_action('wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
}


//function register_my_menus() {
//   register_nav_menus(
//	array(
//	      'top-nav' =>__( 'Top Menu' ),
//	      'platform-menu' =>__( 'Platform Menu' ),
//	      'to-go-menu' =>_( 'To Go Menu')	
//	     )	
//	);
//}

//add_action( 'init', 'register_my_menus' );


function register_my_menus()
{
    if (function_exists('register_nav_menu')) {
        register_nav_menu('platform-nav', 'Platform Menu');
    }
}

add_action('__header', 'display_secondary_menu', 1000, 0);
function display_secondary_menu()
{
    echo(has_nav_menu('platform-nav') ? wp_nav_menu(
            array(
                'theme_location' => 'platform-nav',
                'container_id' => 'platform-nav',
                'container_class' => 'platform-nav'
            )
        ) . '<div class="clearall"></div>' : '');
}

add_action('init', 'register_my_menus');

register_post_type('servers', array(
    'label' => 'Servers',
    'description' => 'Agave Platform server',
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'rewrite' => array('slug' => ''),
    'query_var' => true,
    'has_archive' => true,
    'supports' => array('title',),
    'taxonomies' => array('datacenter',),
    'labels' => array(
        'name' => 'Servers',
        'singular_name' => 'Server',
        'menu_name' => 'Servers',
        'add_new' => 'Add Server',
        'add_new_item' => 'Add New Server',
        'edit' => 'Edit',
        'edit_item' => 'Edit Server',
        'new_item' => 'New Server',
        'view' => 'View Server',
        'view_item' => 'View Server',
        'search_items' => 'Search Servers',
        'not_found' => 'No Servers Found',
        'not_found_in_trash' => 'No Servers Found in Trash',
        'parent' => 'Parent Server')));

register_taxonomy('datacenter',
    array(
        0 => 'servers'),
    array(
        'hierarchical' => false,
        'label' => 'Datacenter',
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => ''),
        'singular_label' => 'Datacenter'));
?>
