<?php

/*
 * Handles custom login styling and config.
 */
class LoginFilters
{

    public function __construct() {}

    public static function init()
    {
        add_filter('login_headerurl', array('LoginFilters', 'c2r_login_logo_url'), 10);
        add_filter('login_headertitle', array('LoginFilters', 'c2r_login_logo_url_title'), 10);
        add_action('login_enqueue_scripts', array('LoginFilters', 'c2r_login_stylesheet'), 10);
        add_action('login_enqueue_scripts', array('LoginFilters', 'c2r_login_stylesheet'), 10);
    }

    /**
     * Updates login logo image to link to site homepage
     * @return string
     */

    public static function c2r_login_logo_url()
    {
        return home_url();
    }


    /**
     * Updates login title to the site title
     * @return mixed
     */
    public static function c2r_login_logo_url_title()
    {
        return get_bloginfo('name');
    }


    /**
     * Adds custom login stylesheets to theme the wordpress login
     * @return mixed
     */
    public static function c2r_login_stylesheet()
    {
        wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/css/login.css');
    }
}

LoginFilters::init();