<?php
/*
Template Name: Server Feed
*/

$numposts = -1;

function yoast_rss_date( $timestamp = null ) {
  $timestamp = ($timestamp==null) ? time() : $timestamp;
  echo date(DATE_RSS, $timestamp);
}

function yoast_rss_text_limit($string, $length, $replacer = '...') { 
  $string = strip_tags($string);
  if(strlen($string) > $length) 
    return (preg_match('/^(.*)\W.*$/', substr($string, 0, $length+1), $matches) ? $matches[1] : substr($string, 0, $length)) . $replacer;   
  return $string; 
}

$posts = query_posts('post_type=servers&showposts='.$numposts);

$lastpost = $numposts - 1;

header("Content-Type: application/rss+xml; charset=UTF-8");
echo '<?xml version="1.0"?>';
?><rss version="2.0">
<channel>
  <title>Agave Server Listing</title>
  <link>https://agaveapi.co/servers/feed</link>
  <description>List of ip addresses making external system requests for the Agave Platform.</description>
  <language>en-us</language>
  <pubDate><?php date('c', strtotime($ps[$lastpost]->post_date_gmt) ); ?></pubDate>
  <lastBuildDate><?php date('c', strtotime($ps[$lastpost]->post_date_gmt) ); ?></lastBuildDate>
<?php foreach ($posts as $post) { ?>
  <item>
    <title><?php echo get_the_title($post->ID); ?></title>
    <link>https://agaveapi.co/servers/feed/<?php echo get_the_title($post->ID); ?></link>
    <description>Server making external system requests for the Agave Platform.</description>
    <pubDate><?php date('c', strtotime($post->post_date_gmt) ); ?></pubDate>
    <guid>https://agaveapi.co/servers/feed/<?php echo get_the_title($post->ID); ?></guid>
  </item>
<?php } ?>
</channel>
</rss>